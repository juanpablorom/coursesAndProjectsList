# 2020 & 2021

## Courses(With certificates)

### Acamica
https://globant.acamica.com

### SoloLearn
https://www.sololearn.com/

---
## Projects

1. University coding projects
2. Matelab applications
3. Personal projects in C++ and Python with Tkinter
4. Youtube Channel's projects

## YouTube Channels to make projects

### Dev Ed (JS)
https://www.youtube.com/channel/UClb90NQQcskPUGDIXsQEz5Q

### Coding the Smart Way (JS)
https://www.youtube.com/channel/UCLXQoK41TOcIsWtY-BgB_kQ

### Ania Kubow (JS)
https://www.youtube.com/channel/UC5DNytAJ6_FISueUfzZCVsw

### One Lone Coder (C++)
https://www.youtube.com/channel/UC-yuWVUplUJZvieEligKBkA

### Bitwise Ar (Arduino)
https://www.youtube.com/c/BitwiseAr/playlists

### Paul McWhorter (Arduino)
https://www.youtube.com/channel/UCfYfK0tzHZTpNFrc_NDKfTA

### Core Electronics (Arduino)
https://www.youtube.com/channel/UCp5ShPYJvi2EA4hLUERa86w

### Free Code Camp (Everyting)
https://www.youtube.com/channel/UC8butISFwT-Wl7EV0hUK0BQ

### Barnacules Nerdgasm (C#)
https://www.youtube.com/user/barnacules1

### Tech Entertaining (Java)
https://www.youtube.com/c/TechEntertaining/playlists

### Web Dev Simplified (JS + HTML + CSS)
https://www.youtube.com/c/WebDevSimplified/videos

### Knowledge Hut (JS & Theory)
https://www.youtube.com/channel/UC3gO0o6xe18NQ-TvJOXXCKA

### Project Worlds (PHP with Laravel and CodeIgniter + Django + Android)
https://www.youtube.com/channel/UCFMkpvtYjSAPXUPjImQK7bg

### PHP Projects
https://www.youtube.com/results?search_query=php+projects

### Python Projects

https://www.youtube.com/results?search_query=tkinter+projects

---
